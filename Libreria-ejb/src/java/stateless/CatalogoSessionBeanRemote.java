/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import entidad.User;
import java.util.Collection;
import javax.ejb.Remote;

/**
 *
 * @author Araujo
 */
@Remote
public interface CatalogoSessionBeanRemote {
        public User addUser(String login, String password, String email);
        public Collection<User> getAllUsers();
        public User buscaUsuario(int id);
        public void actualizaUsuario(User usuario, String login, String password, String email);
        public void eliminaUsuario(int id);
}
