/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;


import entidad.User;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Araujo
 */
@Stateless
public class CatalogoSessionBean implements CatalogoSessionBeanRemote {

    @PersistenceContext(unitName = "Libreria-ejbPU")
    EntityManager em;
    protected User usuario;
    protected Collection<User> ListaUsers;

    /**
     *
     * 
     * @param login
     * @param password
     * @param email
     * @return 
     */
    @Override
    public User addUser(String login, String password, String email)
    {
        usuario = new User(login, password, email);
        em.persist(usuario);
        return usuario;
    }

    
    @Override
    public Collection<User> getAllUsers()
    {
        ListaUsers = em.createNamedQuery("User.findAll").getResultList();
        return ListaUsers;
    }
    
    @Override
    public User buscaUsuario(int id){
    
        usuario = em.find(User.class, id);
        
        return usuario;
    }
    
    @Override
    public void actualizaUsuario(User usuario, String login, String password, String email){
    
        if(usuario!=null){
        
            usuario.setLogin(login);
            usuario.setPassword(password);
            usuario.setEmail(email);
            em.merge(usuario);
        }
    }
    
    @Override
    public void eliminaUsuario(int id){
    
        User usuario = buscaUsuario(id);
        em.remove(usuario);
    }

}

