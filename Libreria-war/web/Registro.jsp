<%-- 
    Document   : forma.jsp
    Created on : 1/10/2014, 02:11:15 PM
    Author     : Araujo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alta de usuarios</title>
    </head>
    <body>
        <h1>Registro de un nuevo usuario:</h1>
        
        <form action="ClienteWeb.jsp" method="POST">
            <table>
                <tr>
                    <td>Introduzca el login:</td>
                    <td><input type="text" name="login" size="40" />
                </tr>
                
                <tr>
                    <td>Introduzca el password:</td>
                    <td><input type="password" name="pass" size="30" /></td>
                </tr>
                
                <tr>
                    <td>Introduzca el email:</td>
                    <td><input type="text" name="email" size="35" /></td>
                </tr>
                
                <tr>
                    <td><input type="submit" value="Registrar" /></td>
                    <td><input type="reset" value="Cancelar" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>
