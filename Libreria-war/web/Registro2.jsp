<%-- 
    Document   : forma.jsp
    Created on : 1/10/2014, 02:11:15 PM
    Author     : Araujo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Eliminacion de usuarios</title>
        
        <script type="text/javascript">
                function confirmarEliminar(formulario) {

                  if(confirm("¿Realmente desea eliminar el registro? \n ID: "+formulario.id.value+"\n Login: "+formulario.login.value
                          +"\n Password: "+formulario.pass.value+"\n Email: "+formulario.email.value)) {

                    return true;

                  }
                  else{
                      return false;
                  }

                }
        </script> 
    </head>
    <body>
        <h1>Eliminacion de usuario:</h1>
        <hr>
        
        <%
            String s0 = new String(request.getParameter("id").getBytes());
            String s1 = new String(request.getParameter("login").getBytes());
            String s2 = new String(request.getParameter("pass").getBytes());
            String s3 = new String(request.getParameter("email").getBytes());
        %>
        <form name="formulario" action="ClienteWeb4.jsp" method="POST" onSubmit="return confirmarEliminar(this);">
            <input type="hidden" name="id" value="<%=s0%>" readonly="readonly" />
            <table>
                
                <tr>
                    <td>Login:</td>
                    <td><input type="text" name="login" value="<%=s1%>" size="40" />
                </tr>
                
                <tr>
                    <td>Password:</td>
                <a href="ClienteWeb2.jsp"></a>
                    <td><input type="password" name="pass" value="<%=s2%>" size="30" /></td>
                </tr>
                
                <tr>
                    <td>Email:</td>
                    <td><input type="text" name="email" value="<%=s3%>" size="35" /></td>
                </tr>
                
                <tr>
                    <td><input type="submit" value="Borrar" /></td>
                    <td><input type="reset" value="Cancelar" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>
